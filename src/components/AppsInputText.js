import React, { Component } from 'react'

export default class AppsInputText extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <input type="text" {...this.props}/>
        )
    }
}
