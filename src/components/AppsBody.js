import React, { Component } from 'react'
import AppNotFoundMessage from './AppNotFoundMessage'
import AppsBodyLists from './AppsBodyLists'
import AppsInputBody from './AppsInputBody'

export default class AppsBody extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div className="note-app__body">
                <AppsInputBody 
                    availableWords={this.props.availableWords}
                    handleTitle={this.props.handleTitle}
                    handleDescription={this.props.handleDescription}
                    handleAdd={this.props.handleAdd}
                    inputTitle={this.props.inputTitle}
                    inputBody={this.props.inputBody}
                />
                <h2>Catatan Aktif</h2>
                {this.props.dataActive.length > 0 && <AppsBodyLists 
                    data={this.props.dataActive} 
                    handleArchiving={this.props.handleArchiving}
                    handleDelete={this.props.handleDelete}
                />}
                {this.props.dataActive.length === 0 && <AppNotFoundMessage/>}
                <h2>Arsip</h2>
                {this.props.dataArchived.length > 0 && <AppsBodyLists 
                    data={this.props.dataArchived}
                    handleArchiving={this.props.handleArchiving}
                    handleDelete={this.props.handleDelete}
                />}
                {this.props.dataArchived.length === 0 && <AppNotFoundMessage/>}
            </div>
        )
    }
}
