import React, { Component } from 'react'
import { getInitialData } from '../utils';
import AppsBody from './AppsBody'
import AppsHeader from './AppsHeader'

export default class NotesApps extends Component {
    constructor(props) {
        super(props);
        this.state = {
          notes: [],
          query: "",
          availableWords: 50,
          inputTitle: '',
          inputBody: ''
        };
    }

    loadInitData(){
        this.setState({notes:getInitialData()})
    }

    onFilter = query => {
        this.setState({query})
    }

    handleTitle = (val) => {
        this.setState({availableWords:50-val.length})
        if(this.state.availableWords>1){
            this.setState({inputTitle:val})
        }
    }

    handleDescription = (val) => {
        this.setState({inputBody:val})
    }

    handleArchiving = (id) => {
        const { notes } = this.state;
        const index = notes.findIndex((x) => x.id === id);
        notes[index].archived = !notes[index].archived;
        this.setState({ notes });
    }
    
    handleDelete = (id) => {
        const notes = this.state.notes.filter((x) => x.id !== id);
        this.setState({ notes });
    }

    handleAdd = (e) => {
        e.preventDefault()
        this.setState((prevState)=>{
            return {
                notes : [
                    ...prevState.notes,
                    {
                        id:+new Date(),
                        title:this.state.inputTitle,
                        body:this.state.inputBody,
                        createdAt: new Date().toISOString(),
                        archived: false
                    }
                ]
            }
        })
        this.handleResetForm()
    }

    handleResetForm = () => {
        this.setState((prevState)=>{
            return {
                ...prevState,
                query: "",
                availableWords: 50,
                inputTitle: '',
                inputBody: ''
            }
        })
    }

    componentDidMount(){
        this.loadInitData()
    }
    render() {
        return (
        <div>
            <AppsHeader 
                onChange={this.onFilter} 
                value={this.state.query}
            />
            <AppsBody 
                dataActive={this.state.notes.filter((x)=>{ return x.archived===false && x.title.includes(this.state.query)})} 
                dataArchived={this.state.notes.filter((x)=>{return x.archived===true && x.title.includes(this.state.query)})}
                handleArchiving={this.handleArchiving}
                handleDelete={this.handleDelete}
                availableWords={this.state.availableWords}
                handleTitle={this.handleTitle}
                handleDescription={this.handleDescription}
                handleAdd={this.handleAdd}
                inputTitle={this.state.inputTitle}
                inputBody={this.state.inputBody}
            />
        </div>
        )
    }
}
