import React, { Component } from 'react'

export default class AppNotFoundMessage extends Component {
  render() {
    return (
        <p className="notes-list__empty-message">Tidak ada catatan</p>
    )
  }
}
