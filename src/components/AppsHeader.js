import React, { Component } from 'react'
import AppsSearch from './AppsSearch'

export default class AppsHeader extends Component {
  constructor(props) {
      super(props);
  }  
  render() {
      return (
        <header className='note-app__header'>
          <h1>Notes</h1>
          <AppsSearch onChange={this.props.onChange} value={this.props.value}/>
        </header>
      )
  }
}
