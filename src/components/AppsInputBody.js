import React, { Component } from 'react'
import AppsInputText from './AppsInputText'

export default class AppsInputBody extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
        <div className="note-input">
            <h2>Buat catatan</h2>
            <form onSubmit={this.props.handleAdd}>
                <p className="note-input__title__char-limit">Sisa karakter: {this.props.availableWords}</p>
                <AppsInputText 
                    className="note-input__title" 
                    placeholder="Ini adalah judul ..." 
                    value={this.props.inputTitle}
                    onChange={(e)=>{
                        this.props.handleTitle(e.target.value)
                    }}
                    required
                />
                <textarea 
                    className="note-input__body" 
                    type="text" 
                    placeholder="Tuliskan catatanmu di sini ..." 
                    value={this.props.inputBody}
                    onChange={(e)=>{
                        this.props.handleDescription(e.target.value)
                    }}
                    required 
                />
                <button type="submit">Buat</button>
            </form>
        </div>
        )
    }
}
