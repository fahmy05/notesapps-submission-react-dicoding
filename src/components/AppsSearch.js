import React, { Component } from 'react'
import AppsInputText from './AppsInputText'

export default class AppsSearch extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div className="note-search">
                <AppsInputText placeholder="Cari catatan ..." value={this.props.value} onChange={(e)=>{
                    this.props.onChange(e.target.value)
                }}/>
            </div>
        )
    }
}
