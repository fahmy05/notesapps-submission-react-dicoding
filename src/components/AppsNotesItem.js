import React, { Component } from 'react'
import { showFormattedDate } from '../utils';

export default class AppsNotesItem extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div className="note-item">
                <div className="note-item__content">
                    <h3 className="note-item__title">{this.props.title}</h3>
                    <p className="note-item__date">{showFormattedDate(this.props.createdAt)}</p>
                    <p className="note-item__body">{this.props.body}</p>
                </div>
                <div className="note-item__action">
                    <button className="note-item__delete-button" onClick={()=>{
                        this.props.handleDelete(this.props.id)
                    }}>Delete</button>
                    <button className="note-item__archive-button" onClick={()=>{
                        this.props.handleArchiving(this.props.id)
                    }}>{this.props.archived?'Pindahkan':'Arsipkan'}</button>
                </div>
            </div>
        )
    }
}
