import React, { Component } from 'react'
import AppsNotesItem from './AppsNotesItem'

export default class AppsBodyLists extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
        <div className="notes-list">
            {this.props.data.map((x,i)=>{
                return (<AppsNotesItem 
                    key={i}
                    id={x.id}
                    title={x.title}
                    createdAt={x.createdAt}
                    body={x.body}
                    archived={x.archived}
                    handleArchiving={this.props.handleArchiving}
                    handleDelete={this.props.handleDelete}
                />)
            })}
        </div>
        )
    }
}
